// run: karma start --browsers Chrome
describe('Rocky.Object', function () {

	var obj;

	beforeEach(function () {
		obj = new Rocky.Object();
	});

	it('should exist', function () {

		expect(Rocky.Object instanceof Object).toBe(true);
	});

	it('should fill when created', function () {

		obj = new Rocky.Object({
			foo: 'bar'
		});

		expect(obj.foo).toEqual('bar');
	});

	it('should not overwrite base methods', function () {

		try {
			obj.set('set', null);
		} catch (e) {}

		expect(obj.set).toBe(Rocky.Object.prototype.set);
	});

	it('should support attribute setter', function () {

		obj.set('foo', 12);
		// obj.set('foo', 'bar');
		// obj.set('foo', [1, 2]);
		// obj.set('foo', {foo: 'bar'});
		// obj.set('foo', new Rocky.Object());

		expect(obj.foo).toEqual(12);
	});

	it('should notify on attribute change [create]', function () {

		obj.observe('foo', function ( change ) {

			expect(change).toEqual({
				path: 'foo',
				newValue: 'bar',
				oldValue: undefined
			});
		});

		obj.set('foo', 'bar');
	});

	it('should notify on attribute change [update]', function () {

		obj.set('foo', 'bar');

		obj.observe('foo', function ( change ) {

			expect(change).toEqual({
				path: 'foo',
				newValue: 'baz',
				oldValue: 'bar'
			});
		});

		obj.set('foo', 'baz');
	});

	it('should notify on attribute change [delete/unset]', function () {

		obj.set('foo', 'bar');

		obj.observe('foo', function ( change ) {

			expect(change).toEqual({
				path: 'foo',
				newValue: undefined,
				oldValue: 'bar'
			});
		});

		obj.unset('foo');
	});

	it('should watch all attributes', function ( done ) {

		obj.observe('*', function ( change ) {

			expect(change).toEqual({
				path: 'elPath',
				newValue: 'bar',
				oldValue: undefined
			});

			done();
		});

		obj.set('elPath', 'bar');
	}, 100);


	// Bad test
	it('should recognize Rocky.Object children', function () {

		obj.set('foo', new Rocky.Object({
			bar: 'baz'
		}));

		expect(obj.foo.bar).toEqual('baz');
	});

	it('should set attribute values with path', function ( done ) {

		obj.set('foo', new Rocky.Object({
			// bar: 'baz' -> With this line enabled, failure
		}));

		obj.observe('foo.bar', function ( change ) {

			expect(change).toEqual({
				path: 'foo.bar',
				newValue: 'baz',
				oldValue: undefined
			});

			done();
		});

		obj.set('foo.bar', 'baz');
	}, 100);

	it('should observe children', function ( done ) {

		obj.set('foo', new Rocky.Object());

		obj.foo.observe('bar', function ( change ) {

			expect(change).toEqual({
				path: 'bar',
				newValue: 'baz',
				oldValue: undefined
			});

			done();
		});

		obj.set('foo.bar', 'baz');
	}, 100);

	it('should update child attributes', function ( done ) {

		obj.set('foo', new Rocky.Object({
			bar: 'baz'
		}));

		obj.observe('foo.bar', function ( change ) {

			expect(change).toEqual({
				path: 'foo.bar',
				newValue: 'zab',
				oldValue: 'baz'
			});

			done();
		});

		obj.set('foo.bar', 'zab');
	}, 100);

	it('should update child attributes and observe trough foo.*', function ( done ) {

		obj.set('foo', new Rocky.Object({
			bar: 'baz'
		}));

		obj.observe('foo.*', function ( change ) {

			expect(change).toEqual({
				path: 'foo.bar',
				newValue: 'zab',
				oldValue: 'baz'
			});

			done();
		});

		obj.set('foo.bar', 'zab');
	}, 100);

	it('should not notify when value does not change', function ( done ) {

		obj.set('foo', new Rocky.Object({
			bar: 'baz'
		}));

		obj.set('*', function () {

			done.fail();
		});

		setTimeout(done, 80);
	}, 100);
});
