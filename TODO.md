# TODO
Links

Parser/compiler should handle assignment expressions `name = 'Name'`
- on-click="name = 'lastname'"

Easy input handling (always bind this to node?)
- <input on-input="name = this.value">

Compile fails with:
compile(Tokenize("a['0']"))
"context.a."0""

compile(Tokenize("a[0]"))
"context.a.0"

Parser fails:
foo.bar['baz']
foo.bar[0]
