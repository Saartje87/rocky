(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
// var binding = new Rocky.Binding(document.body);
// var scope = new Rocky.Scope();
//
// binding.attach(scope);

/*
[
	{
		type: 'textNode',
		node: '{{Node}}',
		value: 'Hello {{user.lastname}}, {{user.name}}',
		links: [user.name', 'user.lastname'],
		trigger: function (context) {
			node.textContent = render(context);
		}
	},
	{
		type: 'attribute',
		node: '{{Node}}',
		values: [{
			name: 'class'
			value: '{{foo}}'
		}],
		link: 'foo',
		trigger: function (context) {
			node.setAttribute(name, render(context));
		}
	},
	{
		// <div on-click="saveUser(user.id)"></div>
		type: 'event',
		node: '{{Node}}',
		value: 'saveUser(user.id)'
		links: [user.id'],
		render: function (context) {
			helper(context);
		}
	}
]

// scope.overwrite({});
*/
// window.Rocky = {};

'use strict';

Object.defineProperty(exports, '__esModule', {
	value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var Binding = (function () {
	function Binding(node) {
		_classCallCheck(this, Binding);

		var list = [];

		this._prepareBinding(node, list);

		this.bindings = list;
	}

	_createClass(Binding, [{
		key: 'attach',
		value: function attach(scope) {

			this.bindings.forEach(function (binding) {

				var trigger = binding.trigger;

				binding.links.forEach(function (link) {

					scope.observe(link, function (change) {

						trigger(scope);
					});
				});

				trigger(scope);
			});
		}
	}, {
		key: '_prepareBinding',
		value: function _prepareBinding(node, list) {

			this._parseNode(node, list);
		}
	}, {
		key: '_parseNode',
		value: function _parseNode(node, list) {

			if (node.nodeType === Node.TEXT_NODE) {
				return this._compileTextNode(node, list);
			}

			if (node.nodeType === Node.ELEMENT_NODE && node.nodeName !== 'SCRIPT') {
				return this._parseElementNode(node, list);
			}
		}
	}, {
		key: '_compileTextNode',
		value: function _compileTextNode(node, list) {

			var content = node.textContent;

			// No need to parse when its an empty string
			if (!content.trim()) return;

			var _parseText2 = this._parseText(content);

			var parts = _parseText2.parts;
			var links = _parseText2.links;

			function render(context) {

				return parts.map(function (value) {

					return typeof value === 'function' ? value(context) : value;
				}).join('');
			}

			list.push({
				type: 'textNode',
				node: node,
				value: content,
				links: links,
				trigger: function trigger(context) {
					node.textContent = render(context);
				}
			});
		}
	}, {
		key: '_parseElementNode',
		value: function _parseElementNode(node, list) {

			var attributes = node.attributes;

			this._parseChildNodes(node, list);

			if (attributes) {
				this._parseAttributes(node, attributes, list);
			}
		}
	}, {
		key: '_parseChildNodes',
		value: function _parseChildNodes(parent, list) {

			for (var node = parent.firstChild; node; node = node.nextSibling) {
				this._parseNode(node, list);
			}
		}
	}, {
		key: '_parseAttributes',
		value: function _parseAttributes(node, attributes, list) {

			for (var i = 0, l = attributes.length; i < l && attributes.hasOwnProperty(i); i++) {

				this._parseAttribute(node, attributes[i], list);
			}
		}
	}, {
		key: '_parseAttribute',
		value: function _parseAttribute(node, attribute, list) {

			var name = attribute.name;
			var value = attribute.value;

			if (name.indexOf('on-') === 0) {
				return this._addEvent(node, name.substr(3), value, list);
			}

			if (value.indexOf('{{') >= 0) {
				return this._compileAttribute(node, name, list, value);
			}
		}
	}, {
		key: '_addEvent',
		value: function _addEvent(node, type, value, list) {

			var tree = window.Tokenize(value);
			var compiled = window.Compile(tree);
			var links = [];

			list.push({
				type: 'event',
				node: node,
				value: value,
				links: [],
				trigger: function trigger(context) {
					node.addEventListener(type, compiled.body.bind(node, context));
				}
			});
		}
	}, {
		key: '_compileAttribute',
		value: function _compileAttribute(node, name, list, value) {

			// No need to parse when its an empty string
			if (!value.trim()) return;

			var _parseText3 = this._parseText(value);

			var parts = _parseText3.parts;
			var links = _parseText3.links;

			function render(context) {

				return parts.map(function (value) {

					return typeof value === 'function' ? value(context) : value;
				}).join('');
			}

			list.push({
				type: 'attributeNode',
				node: node,
				value: value,
				links: links,
				trigger: function trigger(context) {
					node.setAttribute(name, render(context));
				}
			});
		}
	}, {
		key: '_parseText',
		value: function _parseText(content) {

			var parts = [];
			var links = [];
			var matchStart = -1;
			var matchEnd = 0;
			var part;

			// TODO(Saar) Check perf with indexOf('{{|}}', i)
			for (var i = 0; i < content.length; i++) {

				var peek = content[i] + content[i + 1];

				if (peek === '{{') {
					matchStart = i;
					part = content.substring(matchEnd, i);
					if (part) parts.push(part);
				} else if (matchStart >= 0 && peek === '}}') {
					var value = content.substring(matchStart + 2, i);

					matchStart = -1;
					matchEnd = i + 2;

					var tree = window.Tokenize(value);
					var compiled = window.Compile(tree);

					parts.push(compiled.body);
					// TODO Add paths to links, must be unique

					for (var j = 0; j < compiled.paths.length; j++) {
						var path = compiled.paths[j];
						if (links.indexOf(path) === -1) {
							links.push(path);
						}
					}
				}
			}

			part = content.substr(matchEnd);
			if (part) parts.push(part);

			return {
				parts: parts,
				links: links
			};
		}
	}]);

	return Binding;
})();

exports.Binding = Binding;

},{}],2:[function(require,module,exports){
'use strict';

var Rocky = window.Rocky = {
	name: 'Rocky',
	version: '0.0.1',
	Object: require('./mvc/object').RockyObject,
	Binding: require('./binding').Binding
};
// console.log( Rocky );

// import {Crawler} from './Rocky/dom/crawler';
//
// var Rocky = {};
//
// Rocky.element = function RockyElement () {};
// Rocky.initElement = function RockyInitElement () {};
//
// Rocky.dom.walk = function RockyDomWalk () {};
//
// Rocky.Templatize = function ( text ) {
//
// 	return new Template(text);
// };
//
// var tpl = Rocky.Templatize('Hello {{world || "universe"}}');
//
// tpl.observe(new Rocky.Object({world: false}));
// tpl.unobserve();
// tpl.render({world: false});

},{"./binding":1,"./mvc/object":3}],3:[function(require,module,exports){
// TODO(Saar) Batch notify with requestAnimationFrame
"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var RockyObject = (function () {
	function RockyObject() {
		var data = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];

		_classCallCheck(this, RockyObject);

		Object.defineProperty(this, "$listeners", {
			value: []
		});

		this.assign(data);
	}

	/**
  * Assign object
  *
  * @param {Object} source
  */

	_createClass(RockyObject, [{
		key: "assign",
		value: function assign(source) {

			for (var key in source) {
				if (source.hasOwnProperty(key)) {
					this.set(key, source[key]);
				}
			}
		}

		/**
   * Path setter `updatePath('foo.bar', 1234)`
   *
   * @param {String} path
   * @param {mixed} newValue
   */
	}, {
		key: "updatePath",
		value: function updatePath(path, newValue) {

			var parts = path.split(".");
			var attributeName = parts.pop();
			var target = this.find(parts);

			// If( !(target instanceof RockyObject) ) {
			// 	throw `Invalid path ${path}`;
			// }

			target.set(attributeName, newValue);
		}

		/**
   * Resolve path trough given parts
   *
   * @param {Array} parts
   * @return {mixed} value at given path
   */
	}, {
		key: "find",
		value: function find(parts) {

			var reference = this;

			for (var i = 0; i < parts.length; i++) {

				var part = parts[i];

				if (!reference.hasOwnProperty(part)) {
					throw "Path '" + parts.join(".") + "' cannot be resolved";
				}

				reference = reference[part];
			}

			return reference;
		}

		/**
   * Update value
   *
   * @param {String} path
   * @param {mixed} value
   */
	}, {
		key: "set",
		value: function set(path, newValue) {
			var _this = this;

			if (RockyObject.prototype.hasOwnProperty(path)) {
				throw "Rocky.Object.set: Cannot set '" + path + "'";
			}

			if (path.indexOf(".") > -1) {
				return this.updatePath(path, newValue);
			}

			var oldValue = this[path];

			// Value changed?
			if (!(newValue instanceof Object) && newValue === oldValue) {
				return;
			}

			// TODO(Saar) unobserve
			if (newValue instanceof RockyObject) {

				newValue.observe("*", function (change) {

					_this.notify(path + "." + change.path, change.newValue, change.oldValue);
				});
			}

			this[path] = newValue;
			this.notify(path, newValue, oldValue);
		}
	}, {
		key: "unset",
		value: function unset(path) {

			// TODO(Saar) this[path] = undefined. Could we do this more CG/JIT friendly?
			delete this[path];
		}
	}, {
		key: "notify",
		value: function notify(path, newValue, oldValue) {

			this.$listeners.forEach(function (listener) {
				var _path = listener[0];
				var _fn = listener[1];

				if (_path === path || _path === "*" || path.substr(0, path.lastIndexOf(".") + 1) + "*" === _path) {

					_fn({ path: path, newValue: newValue, oldValue: oldValue });
				}
			});
		}
	}, {
		key: "observe",
		value: function observe(path, fn) {

			this.$listeners.push([path, fn]);

			// Return unobserve function
		}
	}]);

	return RockyObject;
})();

exports.RockyObject = RockyObject;

},{}]},{},[2])


//# sourceMappingURL=rocky.js.map