import {Controller} from '../src/mvc/controller';

console.log(Em);

class UserController extends Em.Controller {

	static $model () {
		return new Em.Object({
			name: String,
			lastname: String,
			birthday: String
		});
	}

	constructor () {

		super();

		this.call('Api.user.store').then((user) => {
			this.$extract(user);
		});
	}

	submit ( form ) {

		if( !form.isValis() ) {
			return;
		}

		this.call('Api.User.store', form.getData()).then((response) => {
			this.toast('Your account is updated');
		}, ( responseError ) => {
			this.toast(responseError);
		});
	}

	toast ( message ) {
		this.call('#toast').then((toast) => {
			toast.show(message);
		});
	}
}
