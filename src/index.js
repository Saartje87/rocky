var Rocky = window.Rocky = {
	name: 'Rocky',
	version: '0.0.1',
	Object: require('./mvc/object').RockyObject,
	Binding: require('./binding').Binding
};
// console.log( Rocky );

// import {Crawler} from './Rocky/dom/crawler';
//
// var Rocky = {};
//
// Rocky.element = function RockyElement () {};
// Rocky.initElement = function RockyInitElement () {};
//
// Rocky.dom.walk = function RockyDomWalk () {};
//
// Rocky.Templatize = function ( text ) {
//
// 	return new Template(text);
// };
//
// var tpl = Rocky.Templatize('Hello {{world || "universe"}}');
//
// tpl.observe(new Rocky.Object({world: false}));
// tpl.unobserve();
// tpl.render({world: false});
