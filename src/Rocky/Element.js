var registeredElements = {};

function mixin ( target, source ) {
	for( let key in source ) {
		if( source.hasOwnProperty(key) && !target.hasOwnProperty(key) ) {
			target[key] = source[key];
		}
	}
}

class Element {
	constructor ( node ) {
		console.log(node);

		node.innerHTML = this.template;
	}
}

function element ( name, definition ) {

	name = name.toLowerCase();

	if( registeredElements[name] ) {
		throw `Rocky.element '${name}' already registered`;
	}

	class VMElement extends Element {}

	mixin(VMElement.prototype, definition);

	registeredElements[name] = VMElement;
}

function createElement ( node ) {

	const name = node.nodeName.toLowerCase();

	if( !registeredElements[name] ) {
		throw `Rocky.element '${name}' not registered`;
	}

	new registeredElements[name](node);
}

export {
	element,
	createElement
};
