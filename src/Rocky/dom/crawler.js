class Crawler {

	constructor ( node ) {
		this.walk(node);
	}

	walk ( node ) {

		var childNodes = node.childNodes;

		this.inspect(node);

		for( let i = 0; i < childNodes.length; i++ ) {
			this.walk(childNodes[i]);
		}
	}

	inspect ( node ) {

		var nodeType = node.nodeType;

		if( nodeType === Node.ELEMENT_NODE || nodeType === Node.DOCUMENT_NODE ) {
			console.log('ELEMENT_NODE', node);
			this.inspectAttributes(node);
		}

		if( nodeType === Node.TEXT_NODE && node.textContent && node.textContent.indexOf('{{') >= 0 ) {
			console.log('TEXT_NODE', node.textContent);
		}
	}

	inspectAttributes ( node ) {

		if( !node.hasAttributes() ) {
			return;
		}

		var attributes = node.attributes;

		for( let i = 0; i < attributes.length; i++ ) {

			let name = attributes[i].name;
			let value = attributes[i].value;
			console.log('ATTRIBUTE', name, value);
		}
	}
}

window.addEventListener('DOMContentLoaded', function () {
	new Crawler(document.querySelector('[controller]'));
});

export {Crawler};
