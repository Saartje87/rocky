// TODO(Saar) Batch notify with requestAnimationFrame
export class RockyObject {

	constructor( data = {} ) {

		Object.defineProperty( this, "$listeners", {
			value: []
		} );

		this.assign( data );
	}

	/**
	 * Assign object
	 *
	 * @param {Object} source
	 */
	assign( source ) {

		for ( let key in source ) {
			if ( source.hasOwnProperty( key ) ) {
				this.set( key, source[ key ] );
			}
		}
	}

	/**
	 * Path setter `updatePath('foo.bar', 1234)`
	 *
	 * @param {String} path
	 * @param {mixed} newValue
	 */
	updatePath( path, newValue ) {

		let parts = path.split( "." );
		let attributeName = parts.pop();
		let target = this.find( parts );

		// If( !(target instanceof RockyObject) ) {
		// 	throw `Invalid path ${path}`;
		// }

		target.set( attributeName, newValue );
	}

	/**
	 * Resolve path trough given parts
	 *
	 * @param {Array} parts
	 * @return {mixed} value at given path
	 */
	find( parts ) {

		let reference = this;

		for ( let i = 0; i < parts.length; i++ ) {

			let part = parts[ i ];

			if ( !reference.hasOwnProperty( part ) ) {
				throw `Path '${parts.join( "." )}' cannot be resolved`;
			}

			reference = reference[ part ];
		}

		return reference;
	}

	/**
	 * Update value
	 *
	 * @param {String} path
	 * @param {mixed} value
	 */
	set( path, newValue ) {

		if ( RockyObject.prototype.hasOwnProperty( path ) ) {
			throw `Rocky.Object.set: Cannot set '${path}'`;
		}

		if ( path.indexOf( "." ) > -1 ) {
			return this.updatePath( path, newValue );
		}

		let oldValue = this[ path ];

		// Value changed?
		if ( !( newValue instanceof Object ) && newValue === oldValue ) {
			return;
		}

		// TODO(Saar) unobserve
		if ( newValue instanceof RockyObject ) {

			newValue.observe( "*", ( change ) => {

				this.notify( `${path}.${change.path}`, change.newValue, change.oldValue );
			} );
		}

		this[ path ] = newValue;
		this.notify( path, newValue, oldValue );
	}

	unset( path ) {

		// TODO(Saar) this[path] = undefined. Could we do this more CG/JIT friendly?
		delete this[ path ];
	}

	notify( path, newValue, oldValue ) {

		this.$listeners.forEach( ( listener ) => {
			let _path = listener[ 0 ];
			let _fn = listener[ 1 ];

			if ( _path === path || _path === "*" ||
				path.substr( 0, path.lastIndexOf( "." ) + 1 ) + "*" === _path ) {

				_fn( { path, newValue, oldValue } );
			}
		} );
	}

	observe( path, fn ) {

		this.$listeners.push( [ path, fn ] );

		// Return unobserve function
	}
}
