export class Controller {

}

class UserController extends Controller {

	static before () {
		this.model = new Em.Object({
			name: String,
			lastname: String,
			birthday: String
		});
	}

	constructor () {
		super();
		// console.log(this.$model());
	}
}

window.user = new UserController();

class Templatenizer {

	constructor ( node ) {
		this.walk(node);
	}

	walk ( node ) {

		var childNodes = node.childNodes;

		this.inspect(node);

		for( let i = 0; i < childNodes.length; i++ ) {
			this.walk(childNodes[i]);
		}
	}

	inspect ( node ) {

		var nodeType = node.nodeType;

		if( nodeType === Node.ELEMENT_NODE || nodeType === Node.DOCUMENT_NODE ) {
			console.log('ELEMENT_NODE', node);
			this.inspectAttributes(node);
		}

		if( nodeType === Node.TEXT_NODE && node.textContent && node.textContent.indexOf('{{') >= 0 ) {
			console.log('TEXT_NODE', node.textContent);
		}
	}

	inspectAttributes ( node ) {

		if( !node.hasAttributes() ) {
			return;
		}

		var attributes = node.attributes;

		for( let i = 0; i < attributes.length; i++ ) {

			let name = attributes[i].name;
			let value = attributes[i].value;
			console.log('ATTRIBUTE', name, value);
		}
	}
}

window.addEventListener('DOMContentLoaded', function () {
	new Templatenizer(document.querySelector('[controller]'));
});

class EmInput extends Em.Element {

	static get templateUrl () {
		return '';
	}
}

Rocky.Element('em-input', {
	template: `<input type="{{name}}"><content select="*"></content>`,
	templateUrl: '/template.html',

	// Converted to `Rocky.Object`
	properties: {
		label: String,
		bind: String,
		required: String
	}
});

// <div controller="UserController as user"></div>
// Behaves like Trait
Rocky.Attribute('controller', {

	// Converted to `Rocky.Object`
	properties: {
		controller: String
	},

	controllerChange ( newValue, oldValue ) {
		// Scope element
		this.scope(this, new Rocky.controller[newValue]());
	}
});
