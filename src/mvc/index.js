import {_Array} from './array';
import {_Object} from './object';
import {Controller} from './controller';

console.log(_Array);

var Em = {
	Array: _Array,
	Object: _Object,
	Controller
};

export {Em};
