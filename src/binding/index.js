// var binding = new Rocky.Binding(document.body);
// var scope = new Rocky.Scope();
//
// binding.attach(scope);

/*
[
	{
		type: 'textNode',
		node: '{{Node}}',
		value: 'Hello {{user.lastname}}, {{user.name}}',
		links: [user.name', 'user.lastname'],
		trigger: function (context) {
			node.textContent = render(context);
		}
	},
	{
		type: 'attribute',
		node: '{{Node}}',
		values: [{
			name: 'class'
			value: '{{foo}}'
		}],
		link: 'foo',
		trigger: function (context) {
			node.setAttribute(name, render(context));
		}
	},
	{
		// <div on-click="saveUser(user.id)"></div>
		type: 'event',
		node: '{{Node}}',
		value: 'saveUser(user.id)'
		links: [user.id'],
		render: function (context) {
			helper(context);
		}
	}
]

// scope.overwrite({});
*/
// window.Rocky = {};

export class Binding {

	constructor (node) {

		var list = [];

		this._prepareBinding(node, list);

		this.bindings = list;
	}

	attach (scope) {

		this.bindings.forEach(binding => {

			var trigger = binding.trigger;

			binding.links.forEach(link => {

				scope.observe(link, (change) => {

					trigger(scope);
				});
			});

			trigger(scope);
		});
	}

	_prepareBinding (node, list) {

		this._parseNode(node, list);
	}

	_parseNode (node, list) {

		if( node.nodeType === Node.TEXT_NODE ) {
			return this._compileTextNode(node, list);
		}

		if( node.nodeType === Node.ELEMENT_NODE && node.nodeName !== 'SCRIPT' ) {
			return this._parseElementNode(node, list);
		}
	}

	_compileTextNode (node, list) {

		var content = node.textContent;

		// No need to parse when its an empty string
		if( !content.trim() ) return;

		var {parts, links} = this._parseText(content);

		function render (context) {

			return parts.map(function (value) {

				return (typeof value === 'function') ?
					value(context) :
					value;
			}).join('');
		}

		list.push({
			type: 'textNode',
			node: node,
			value: content,
			links: links,
			trigger: function (context) {
				node.textContent = render(context);
			}
		});
	}

	_parseElementNode (node, list) {

		let attributes = node.attributes;

		this._parseChildNodes(node, list);

		if( attributes ) {
			this._parseAttributes(node, attributes, list);
		}
	}

	_parseChildNodes (parent, list) {

		for( let node = parent.firstChild; node; node = node.nextSibling ) {
			this._parseNode(node, list);
		}
	}

	_parseAttributes (node, attributes, list) {

		for( var i = 0, l = attributes.length; i < l && attributes.hasOwnProperty(i); i++ ) {

			this._parseAttribute(node, attributes[i], list);
		}
	}

	_parseAttribute (node, attribute, list) {

		let name = attribute.name;
		let value = attribute.value;

		if( name.indexOf('on-') === 0 ) {
			return this._addEvent(node, name.substr(3), value, list);
		}

		if( value.indexOf('{{') >= 0 ) {
			return this._compileAttribute(node, name, list, value);
		}
	}

	_addEvent (node, type, value, list) {

		var tree = window.Tokenize(value);
		var compiled = window.Compile(tree);
		var links = [];

		list.push({
			type: 'event',
			node: node,
			value: value,
			links: [],
			trigger: function (context) {
				node.addEventListener(type, compiled.body.bind(node, context));
			}
		});
	}

	_compileAttribute (node, name, list, value) {

		// No need to parse when its an empty string
		if( !value.trim() ) return;

		var {parts, links} = this._parseText(value);

		function render (context) {

			return parts.map(function (value) {

				return (typeof value === 'function') ?
					value(context) :
					value;
			}).join('');
		}

		list.push({
			type: 'attributeNode',
			node: node,
			value: value,
			links: links,
			trigger: function (context) {
				node.setAttribute(name, render(context));
			}
		});
	}

	_parseText (content) {

		var parts = [];
		var links = [];
		var matchStart = -1;
		var matchEnd = 0;
		var part;

		// TODO(Saar) Check perf with indexOf('{{|}}', i)
		for( let i = 0; i < content.length; i++ ) {

			let peek = content[i]+content[i + 1];

			if( peek === '{{' ) {
				matchStart = i;
				part = content.substring(matchEnd, i);
				if( part ) parts.push(part);
			} else if ( matchStart >= 0 && peek === '}}' ) {
				let value = content.substring(matchStart + 2, i);

				matchStart = -1;
				matchEnd = i + 2;

				var tree = window.Tokenize(value);
				var compiled = window.Compile(tree);

				parts.push(compiled.body);
				// TODO Add paths to links, must be unique

				for ( let j = 0; j < compiled.paths.length; j++ ) {
					let path = compiled.paths[j];
					if( links.indexOf(path) === -1 ) {
						links.push(path);
					}
				}
			}
		}

		part = content.substr(matchEnd);
		if( part ) parts.push(part);

		return {
			parts,
			links
		};
	}
}
